region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

enable_classiclink = "false"

#enable_classiclink_dns_support = "false"
environment = "Dev" 

tags = {
  Owner-Email     = "rstdaupdates@gmail.com"
  Managed-By      = "Terraform"
  Billing_Account = "590183852323"
}

account_no = 590183852323

master-username = "admin"

master-password = "adminBanjo"

ami = "ami-053b0d53c279acc90"

keypair = "terra"

ami-bastion = "ami-0679aeadfd126acc1"

ami-nginx = "ami-05f5b4620119402a7"

ami-sonar = "ami-07b2c82b83a016c31"

ami-web = "ami-0ff8a372b54be1c9b"

#End of tfvars file 