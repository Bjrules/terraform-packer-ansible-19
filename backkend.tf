# terraform {
#   backend "s3" {
#     bucket         = "sledge-25"
#     key            = "global/s3/terraform.tfstate"
#     region         = "us-east-1"
#     dynamodb_table = "terraform-locks"
#     encrypt        = true
#   }
# }

# Use this configuration is as defined by the terraform cloud .
terraform {
  backend "remote" {
    organization = "BBwestern"

    workspaces {
      name = "terraform-packer-ansible-19"
    }
  }
}